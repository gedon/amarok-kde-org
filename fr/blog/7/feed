<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://amarok.kde.org"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>hydrogen&#039;s blog</title>
 <link>https://amarok.kde.org/fr/blog/7</link>
 <description>

Amarok is a powerful music player for Linux, Unix and Windows with an intuitive interface. It makes playing the music you love and discovering new music easier than ever before - and it looks good doing it!

Discover what Amarok has to offer. And see where we want to take it.</description>
 <language>fr</language>
<item>
 <title>The best feature of Amarok...</title>
 <link>https://amarok.kde.org/fr/node/604</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;It seems in every version of Amarok 2 we have released there is at least one comment that sounds something like:&lt;br /&gt;
  &quot;But, the best feature of amarok 1.4 is still missing! Without --Insert one of the zillion and a half features Amarok 1.4 had-- Amarok 2.0 is useless!&quot; &lt;/p&gt;
&lt;p&gt;It is interesting for me to observe just how many &quot;best features&quot; Amarok 1.4 had, and also to observe exactly how many different ways Amarok was used.  One of the issues with Amarok 1 development was that Amarok was trying to be a jack of all trades, and not really mastering any of them.  This was well and good for the average Linux user, many of whom, when Amarok 1 was originally created, had no problem digging into the source to add their own little tweaks, features, or bug fixes.  However, it is a problem for people that just want to listen to music, and leave the text editors and compilers at the door. With Amarok 2, our goal was not just to have features, but to perfect them.&lt;/p&gt;
&lt;p&gt;To perfect features, we needed to limit our scope.  Amarok 1 included a lot of dump-and-run code, where a large feature was submitted by an interested user, the code was committed and shipped, and then we never heard from the author again.  This was the case with many of the media devices, and many of the engine backends.  &lt;/p&gt;
&lt;p&gt;We rely on phonon in amarok 2, so the engine issue is no longer one we have control over, although we are suffering from the results of incomplete phonon engines.  While it is all sorts of nice to get backends for windows and mac for free, they come with many issues still, and we are constantly closing bugs reported by users that use the gstreamer phonon backend, as it is extremely buggy and incomplete.  Unfortunately, many distributions ship this by default (due to patent issues) and as a result Amarok gets a reputation as being a lot less stable than it actually is on these distributions.  Hopefully this issue is resolved in the future.&lt;/p&gt;
&lt;p&gt;With regards to portable media devices, the ball is still in our park.  Alejandro (xevix) had a summer of code project for portable media device support, and is working now on generalizing the code so that it will be fairly simple to add new devices.  The shared infrastructure that Amarok 2 is built on (The Meta/Collection architecture) provides a familiar interface, so that even if Alejandro runs out of time (or interest) in the future, the code will be clear and understandable to whoever might pick up the pieces.  The next step will be to readd additional media devices to Amarok 2 using this infrastructure.  This should happen for 2.1 or 2.2, depending on various factors.&lt;/p&gt;
&lt;p&gt;As we are constantly being reminded, perfection is not a fire and forget process.  Code constantly improves (we hope) and mistakes we made in previous versions influence the next iteration that comes.  This is quite visually apparent with the playlist.  The Amarok 1.4 playlist was incredible to many people.  It had a a lot of functionality and all sorts of nifty features, but at the same time, it was entirely unmaintainable by the time Amarok 1.4 ended. See &lt;a href=&quot;http://websvn.kde.org/*checkout*/tags/amarok/1.4.9/multimedia/amarok/src/playlist.cpp?revision=795162&amp;amp;content-type=text%2Fplain&quot;&gt;this file&lt;/a&gt; if you don&#039;t believe me.  For Amarok 2 we knew the playlist was important, but we wanted to start over, using the new model/view features in Qt4, and make it look more visually appealing.  Ian&#039;s adventures are &lt;a href=&quot;http://amarok.kde.org/blog/archives/457-Status-of-QGraphicScenes-and-Delegates.html&quot;&gt;well&lt;/a&gt; &lt;a href=&quot;http://amarok.kde.org/blog/archives/454-A-Cry-for-Help-Combining-GraphicsView-and-Interview.html&quot;&gt;documented&lt;/a&gt;.  Nikolaj&#039;s adventures leading up to 2.0 were also well documented.  The system that was created for 2.0 was a far cry from perfect, (the various paint methods made me cry on more than one occasion) but it was an important step on the road to this whole perfection thing.  As Nikolaj more recent &lt;a href=&quot;http://amarok.kde.org/blog/archives/858-From-the-Post-2.0.0-Git-Vaults,-Part-2,-The-Playlist-Evolved.html&quot;&gt;blogged&lt;/a&gt;, Amarok 2.1 is moving a lot closer to the ideal.  I&#039;m sure it will have issues, but with testing and use it will hopefully evolve into what we originally intended, a kick-ass feature that puts the Amarok 1.4 playlist to shame.&lt;/p&gt;
&lt;p&gt;This has wandered pretty far afield from the point I didn&#039;t start with, but I suppose what I&#039;m getting at is the same thing that we keep saying, and because I like to hit dead horses, I&#039;ll say it again.  Amarok 2 is going to have many of the best features that Amarok 1 had.  It will not have all of them.  Our goal is to make a music player that is fun and enjoyable to use for a majority of the people.  If this means omitting some esoteric features, then so be it.  We also want a product that is fun and enjoyable for developers to work on, and that also provides incentive to not include the &quot;fire-and-forget&quot; code that composed many of the less-central features of Amarok 1.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Wed, 21 Jan 2009 00:50:39 +0000</pubDate>
 <dc:creator>hydrogen</dc:creator>
 <guid isPermaLink="false">604 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/fr/node/604#comments</comments>
</item>
<item>
 <title>It was just a beginning</title>
 <link>https://amarok.kde.org/fr/node/597</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;This is the first time I&#039;ve actually gotten around to posting anything on the internet.  I&#039;ve been working on Amarok since we started on the 2.0 adventure, and have stuck my nose in most of Amarok since then.  I&#039;m hoping to write a series of posts about some of the cool things in Amarok 2, and some of the cooler things to come.  Today I&#039;m going to preview 2.0.1, because as we said earlier, 2.0 was just a beginning, and the evolution is already going full speed. &lt;/p&gt;

&lt;p&gt;Amarok&#039;s code was in a feature freeze for a while before 2.0 was released.  There were all sorts of changes in the code, and all sorts of annoying bugs that needed to be fixed.  Fixing bugs is no where near as fun as adding new features, and we needed to stop adding features to let the code settle down.  This left Amarok 2 missing some very useful features that Amarok 1.4 had.  The good news is, many of these features have returned in 2.0.1&lt;/p&gt;

&lt;p&gt;2.0.1 brings back searching in the playlist, as Nikolaj wrote &lt;a href=&quot;http://amarok.kde.org/blog/archives/854-Amarok-2-playlist-searching.html&quot;&gt;here&lt;/a&gt;.  Since then, Nikolaj has taken it a step further and added filtering as well as searching, which provides the opportunity to use it much as it was used in 1.4.  Seb has also merged his support for queueing, another big feature that was missing in 2.0.   In addition, &quot;Stop after Current track&quot; functionality is slowing making it&#039;s way back into svn.  This is one of those features that is much more useful that it sounds and I&#039;ve been missing it since it disappeared during 2.0 development.&lt;/p&gt;

&lt;p&gt;In addition, media device support is improving.  Alejandro has been making all sorts of improvements to ipod and mtp device support.  We have plans to add support for generic media devices, and probably others as well.  This is a great opportunity for new contributers, as this code is fairly self-contained and requires a device to use (and test) it.  If anyone is interested in contributing to Amarok, this would be a great place to start.&lt;/p&gt;

&lt;p&gt;I&#039;ve re-added custom sorting to the collection browser as well.  This is something that I don&#039;t use all that much, but it&#039;s nice to be able to change the sorting order in the collection.  In addition, we&#039;ve fixed somewhere between fifteen and twenty-five bugs already, and will probably fix more before 2.0.1 is officially released in a few weeks.&lt;/p&gt;

&lt;p&gt;The other place my focus has been recently is on the windows version of Amarok.  It&#039;s definitely usable now.  I&#039;ve fixed most of the annoying issues, and I can use it as my primary music player for local and remote music.  I&#039;ll end this post with a screenshot of Amarok 2.0.1 on windows, and encourage you to try it!&lt;/p&gt;

&lt;img src=&quot;http://www.notyetimplemented.com/images/amarok-2.0.1-blog1.jpg&quot; /&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Sat, 27 Dec 2008 17:58:41 +0000</pubDate>
 <dc:creator>hydrogen</dc:creator>
 <guid isPermaLink="false">597 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/fr/node/597#comments</comments>
</item>
</channel>
</rss>
